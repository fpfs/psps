from sniffer.api import * # import the really small API
import os, termstyle

# you can customize the pass/fail colors like this
pass_fg_color = termstyle.green
pass_bg_color = termstyle.bg_default
fail_fg_color = termstyle.red
fail_bg_color = termstyle.bg_default

# All lists in this variable will be under surveillance for changes.
watch_paths = ['.', 'tests/']

# this gets invoked on every file that gets changed in the directory. Return
# True to invoke any runnable functions, False otherwise.
#
# This fires runnables only if files ending with .py extension and not prefixed
# with a period.
@file_validator
def py_files(filename):
    return filename.endswith('.py') and not os.path.basename(filename).startswith('.')

# This gets invoked for verification. This is ideal for running tests of some sort.
# For anything you want to get constantly reloaded, do an import in the function.
#
# sys.argv[0] and any arguments passed via -x prefix will be sent to this function as
# it's arguments. The function should return logically True if the validation passed
# and logicially False if it fails.
#
# This example simply runs nose.
@runnable
def execute_nose(*args):
    import nose
    from pathlib import Path
    for watched_dir in watch_paths:
        watched_dir = Path(watched_dir)
        for f in list(watched_dir.glob('**/*.py')):
            exec(Path(f).read_text())
            return nose.run(argv=list(args))

@runnable
def list_tags(*args):
    from pathlib import Path
    
    tags = ['TODO', 'FIXME', 'NOTE', 'NOTES', 'XXX']

    foundfile = False
    for watched_dir in watch_paths:
        watched_dir = Path(watched_dir)
        fileList = list(watched_dir.glob('**/*.py'))

        # Check for any lines with tags present
        for afile in fileList:
            if str(afile) == 'scent.py':
                continue
            with open(afile) as file_buffer:
                linenum = 0
                count = 0
                for line in file_buffer:
                    linenum += 1
                    index = -1
                    for tag in tags:
                        if tag in line:
                            index = line.find(tag)
                    if index != -1:
                        if count == 0:
                            print(f'{afile}')
                            print('----------')
                        foundfile = True
                        count +=1
                        print(f'line {linenum}: {line[index:]}')
                if count > 0:
                    print(f'{count} tags found in {afile}')
    return not foundfile

