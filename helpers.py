'''
Helper classes and methods for psps.py
'''
## External Modules
from random import randrange
from sys import exit
import argparse

## Local Modules
import config

###############
### Classes ###
###############

class SmartFormatter(argparse.HelpFormatter):
    '''
    Format any helptext that starts with R| to allow newlines
    '''
    def _split_lines(self, text, width):
        if text.startswith('R|'):
            return text[2:].splitlines()  
        # this is the RawTextHelpFormatter._split_lines
        return argparse.HelpFormatter._split_lines(self, text, width)

class Player():
    def __init__(self,name,priority,snub):
        self.name       = str(name)
        self.tabs       = '\t'*config.tabs 
        self.priority   = int(priority)
        self.snub       = int(snub)
        self.skip       = False
        self.selected   = False
        self.roll       = 0
        self.target     = 0
        return None

    def tabulate(self):
        if ((int(len(self.name)/8)+2)) == config.tabs:
            self.tabs = '\t'*(config.tabs-1)
        else:
            self.tabs = '\t'*config.tabs
        return None

    def new_target(self):
        return self.priority + (config.snubWeight * self.snub)

    def sel_roll(self):
        '''
        Roll for the player vs their target, return True if they pass, else False
        '''
        self.roll       = roll()
        self.skip       = 1
        self.target     = self.new_target()
        self.selected   = self.roll <= self.target
        return self.selected
    
    def sel_str(self): 
        '''
        Return a string with the result of a player's roll
        '''
        output = '{n}{tab}rolls: {r:3} <= {t:3} '.format(n=self.name,tab=self.tabs,r=self.roll,t=self.target)
        
        if self.selected:
            playing = len(config.playersSelected) +1
            output += f'pass! {playing}/{config.availableSlots}'
        else:
            #this depends on the snub value to be added outside of this!
            output += f'fail! snub+1'
        
        return output

#################
### Functions ###
#################
def roll(sides=0):
    '''
    Roll a dice with n sides!
    '''
    if sides == 0:
        sides = config.diceSides

    return randrange(1,sides+1)

def coin_flip(valueA, valueB):
    '''
    Select either valueA or valueB, depending on a coin flip
    '''
    if roll(2) == 1:
        return valueA
    else:
        return valueB

def build_ply_list(prelist):
    '''
    Populate player list with appropriate data
    '''
    lList=''
    pli= []
    for line in prelist:
        line = line.partition('#')[0]
        line = line.rstrip()
        #add players line by line
        if line != '':
            l = line.split(',')
            
            name = str(l[0])
            pri  = config.defaultPriority
            snub = config.defaultSnubs

            if len(l) == 1:
                pass
            elif len(l) == 2:
                pri = int(l[1])     
            elif len(l) == 3:
                snub = int(l[2])
            else:
                exit(f'Formatting invalid before generating player list')
            pli.append(Player(name,pri,snub))
    return pli

def update_tabs(pli): 
    '''
    Read the length of the player list and decide how many tabs we need to keep things tidy.
    '''
    tabsForName=[]
    for ply in pli:                                       
        tabsForName.append(int(len(ply.name)/8)+2)
    return max(tabsForName)

def tabulate_players(pli):
    for ply in pli:
        ply.tabulate()
    return None

def choose_players(pli):
    '''
    run through the process of selecing players from the provided player list
    '''
    sli = [] # selected players
    eli  = [] # excluded players

    rnd = 1

    # just set selection to player list if there's free slots
    if config.playerCount < config.availableSlots:
        print("No excess players, no selection rolls needed.")
        for ply in pli:
            sli.append(ply.name)
        return eli, sli
    
    print(f'\nBeginning selection rolls\n{config.spacer}')
    rnd = 1
    # populate the selected players
    while len(sli) < config.availableSlots:

        # Move to next round if players all set to skip
        all_skip = all(ply.skip == True for ply in pli)
        if (all_skip):
            print(f'-- End of round {rnd} --')
            rnd = rnd + 1
            for ply in pli:
                ply.skip = False
            continue
         
       
        # select a player randomly
        plysLeft = len(pli)
        selPly = pli[roll(plysLeft)-1]
        if not selPly.skip:
            selPly.skip = 1
            # Begin rolling for player
            if selPly.sel_roll():
                sli.append(selPly.name)
                pli.remove(selPly)
                print(selPly.sel_str())
            else:
                selPly.snub += 1 

    print(f'\nAll player slots filled!\n')

    # Populate excluded players
    for ply in pli:
        if ply not in sli:
            eli.append(ply)
    
    return sli, eli

def intro():
    '''
    Build and return an introduction string.
    '''
    excludeAmount= config.playerCount - config.availableSlots
    print(
            f'{config.bigSpacer}\n'
            f'Pathfinder Society Player Selection ({config.version})\n'
            f'{config.spacer}\n'
            f'Module: {config.moduleName}\n'
            f'Player Signups: {config.playerCount} | Slots Available: {config.availableSlots}\n'
            f'Dice used for rolls: 1d{config.diceSides}\n'
            f'{config.bigSpacer}\n'
            )
    return None

def list_players(pli):
    print(f'Player Candidates:\n{config.spacer}')
    for ply in pli:
        print(f'{ply.name}{ply.tabs}priority:{ply.priority} snubs:{ply.snub}')
    return

def summary():
    '''
        prints a final results and summary of the player selection
    '''
    if len(config.playersExcluded) != 0:
        print(f'Agents not selected:\n{config.spacer}')         
        for ply in config.playersExcluded:
            print(f'{ply.name}{ply.tabs} snub count = {ply.snub}')
    
    print(f'\n{config.bigSpacer}\n\nCongratulations agents! Our adventuring party is\n{config.spacer}')
    for player in config.playersSelected:
        print(player)
    return None

def input_int(qs):
    '''
        returns an empty string if fed nothing, otherwise gives an int
    '''
    s = 'x'

    while not s.isdigit():
        s = input(qs)
        if s == '':
            return s
        elif not s.isdigit():
            print("Please provide a full number")

    return int(s) # returns a empty string if provided nothing

def wizard():
    '''
        guides user in creating arguments to use then runs with the arguments
    '''

    argPlys     = []
    argModule   = config.moduleName
    argCount    = config.availableSlots
    argPri      = config.defaultPriority
    argSnub     = config.snubWeight
    argSides    = config.diceSides   

    yesList=['y','yes']
    noList=['n','no']

    fullArgsStr = ''

    print("A series of questions will be asked to help you use this tool.")
    print("Anything contained in parentheses indicates the value used if you enter nothing.")
    
    answer = input (f'Module Name ({argModule}): ')
    if answer != '':
        argsStr = f'-n "{answer}" '
        fullArgsStr += argsStr
        print(f'Added optional argument: {argsStr}')
        argModule = answer

    answer = input_int(f'Available Player Slots ({argCount}): ')
    if answer != '':
        argsStr = f'-c {answer} '
        fullArgsStr += argsStr
        print(f'Added optional argument: {argsStr}')
        argCount = int(answer)
    
    while True:
        answer = input(f'Do you want to change any default variables? Y/N (N): ')
        if answer in yesList:
            answer = input_int(f'Default priority value players are to roll under ({argPri}): ')
            if answer != '':
                argsStr = f'-p {answer} '
                fullArgsStr += argsStr
                print(f'Added optional argument: {argsStr}')
                argPri = int(answer)

            answer = input_int(f'Additional Priority value added by each Snub ({argSnub}): ')
            if answer != '':
                argsStr = f'-s {answer} '
                fullArgsStr += argsStr
                print(f'Added optional argument: {argsStr}')
                argSnub = int(answer)

            answer = input_int(f'How many sides the dice we roll has ({argSides}): ')
            if answer != '':
                argsStr = f'-d {answer} '
                fullArgsStr += argsStr
                print(f'Added optional argument: "{argsStr}')
                argSides = int(answer)

            break
        elif answer in noList or answer == '':
            break
        else:
            print("I only understand the following inputs for this question")
            print(f'Interpereted as Yes: {yesList}')
            print(f'Interpereted as No:  {noList}')
            print("Please try again.")
        
    index = 0
    print("We will now begin to populate the player list.")
    print("If no more players are to be added, answer with nothing when asked for the player's name")
    while True:
        answer = input(f'Player Name: ')
        if answer != '':
            
            plyName = answer
            plyPri  = argPri
            plySnb  = argSnub
            
            plyArg = plyName  # string that would be typed as an argument for this script

            answer = input_int(f'Priority to roll under ({argPri}): ')
            if answer != '':
                plyArg += f',{answer}'
                plyPri = int(answer)

            answer = input_int(f'Snubs this player has (0): ')
            if answer != '':
                if ',' not in plyArg:
                    plyArg += f',{plyPri}'
                plyArg += f',{answer}'
                plySnb = int(answer)
                        
            argPlys.append(plyArg)
            print(f'Added positional argument: "{plyArg}"')
            fullArgsStr += f'"{plyArg}" '
            index += 1
        else:
            break
    
    # Print argument string then run
    print('Wizard complete, We will now run the following command after you press enter.')
    print(f'./psps.py {fullArgsStr}')
    input()
    print('Wizard completed, passing above arguments to be parsed.\n\n')
    return argPlys, argModule, argCount, argPri, argSnub, argSides

if __name__ == "__main__":
    exit("This file is a library module and can not be run directly.")

