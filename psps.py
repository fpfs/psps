#!/usr/bin/env python3
'''
A over-complicated script for the simple task of player selection for a small pathfinder society branch.
'''
full_name="Pathfinder Society Player Selection"
__version__='1.4.1'

## External modules
import argparse
from sys import argv
## Internal modules
import config
import helpers

## Set up argument parsing
def parseArguments():
    '''
    Set and parse our arguments
    '''
    parser = argparse.ArgumentParser(add_help=False, formatter_class=helpers.SmartFormatter,\
        description=__doc__)
    
    # required arguments
    parser.add_argument("playerName", nargs='*', default='',\
        help =  "R|list of player names, optionally paired with their priority and snub count\n"
                "if priority or more values are attached to an entry, it must be put in quotes\n"
                "order of grouping is always Name, Priority, Snub\n\n"
                "examples:\n"
                "psps.py player1 player2 player3\n"
                "psps.py 'myplayer,50,0'\n"
                "psps.py 'yip,70,0' 'yap,50,0' 'yop,25,1'\n"
                "psps.py 'egg,80' kobold 'simon,10,5' ")
    
    # operations
    operations=parser.add_argument_group('operations')
    operations.add_argument("-h", "--help", action="help",\
        help=   "show this help message and exit")
    operations.add_argument("-w", "--wizard", action="store_true",\
        help=   "invoke the wizard to build and run a playerlist after asking you some questions")
    operations.add_argument("-e", "--examplefile", action="store_true",\
        help=   "R|create a example file to be read as a player list as example_list.txt\n"
                "this will overwrite any exsisting example_list.txt file!") 
    operations.add_argument("-f", "--file", type=str, default='',\
        help=   "read playerlist from provided filename")
    
    # optional arguments
    optional=parser.add_argument_group('optional arguments')
    optional.add_argument("-n", "--modulename", type=str,\
        help=   "change the name of the module being run\n"
                "default is " + str(config.moduleName)) 
    optional.add_argument("-c", "--playercount", type=int,\
        help=   "change the amount of available player slots for the module\n"
                "default is " + str(config.availableSlots))
    optional.add_argument("-p", "--priorityvalue", type=int,\
        help=   "change the priority value to roll under, default is " + str(config.defaultPriority))
    optional.add_argument("-s", "--snubweight", type=int,\
        help=   "change the snub weight, which is priority value to add per snub, default is " + str(config.snubWeight))
    optional.add_argument("-d", "--dicesides", type=int,\
        help=   "R|change how many sides the die rolled has\n"
                "default is " + str(config.diceSides) +", which makes 1d"+str(config.diceSides)+"\n"
                "if a priority value is higher than the dice rolled the player will be automatically selected")

    args = parser.parse_args(args=None if argv[1:] else ['--help'])

    return args

## Main section, runs here first
if __name__ == "__main__":
    '''
    set runtime variables, decide mode of operation, run player selection and show the user the result 
    '''
    # prepare our arguments
    args = parseArguments()

    
    # Check for early operations
    if args.examplefile:
        with open('example_list.txt',mode='w') as f:
            f.write(config.examplePlayerFile)
        exit(0)
    elif args.wizard:
        args.playerName, args.modulename, args.playercount, args.priorityvalue, args.snubweight, args.dicesides = helpers.wizard()
    
    # arguments
    if args.modulename:
        config.moduleName = args.modulename
    if args.playercount:
        config.availableSlots = args.playercount
    if args.priorityvalue:
        config.defaultPriority=args.priorityvalue
    if args.snubweight:
        config.snubWeight = args.snubweight
    if args.dicesides:
        config.diceSides = args.dicesides

    # CLI Mode
    if args.playerName:
        config.playerList = helpers.build_ply_list(args.playerName) 
    # File Mode
    elif args.file != '':
        file_buffer = open(args.file)
        config.playerList = helpers.build_ply_list(file_buffer)
    else:
        print("No positional arguments or operations provided. (use -h for help)")
        exit(2)

    # update some variables now that we have the required data
    config.tabs = helpers.update_tabs(config.playerList)
    helpers.tabulate_players(config.playerList)
    config.playerCount = len(config.playerList)

    # introduce ourself to the user
    helpers.intro()
    helpers.list_players(config.playerList)

    # begin selection rolls
    config.playersSelected, config.playersExcluded = helpers.choose_players(config.playerList)

    # print a summary of who was and was not selected
    helpers.summary()

