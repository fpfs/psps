
# psps.py (Pathfinder Society Player Selector)
A Python script for selecting players to play when there are more signups then slots.

Created and refined as a learning tool, if you see anything bad(tm), please let me know so I can fix it.

## Features
 - Randomly select who gets to play from a list of players
 - Provide the list of players as arguments when calling the script
 - Load the list of players from a file
 - A helpful Wizard to guide you through the process if you need it
 - Adjustable player selection values
 - Verbose output! We don't want anything hidden

## Dependencies / Required Applications
 - Python3.9.5 or later
 - psps.py must be run in a Command Line Interface

---
# Installation
To use psps.py, you must download and install it on your system.

## Install Dependencies / Required Applications
psps.py requires Python 3 to run, and must be run in a Command Line Interface

### Install Python 3
You will need to download and install the latest version of **Python 3**:
 - **Linux**: Install via your package repository, refer to your distributions documentation for more information.
 - **Mac and Windows**: [Official Python Downloads Page](https://www.python.org/downloads/)

### Install Command Line Interface
You will also need a Command Line Interface. Your operating system will have one installed by default, but you may install a different one if you desire.

### Command Line Interface Applications by OS
 - **Linux**: xterm, GNOME Terminal, konsole, rxvt, (there are thousands...)
 - **Mac**: Terminal, iTerm
 - **Windows**: PowerShell, cmd, mintty


## Install psps.py
**Without Git:** (recommended for non-technical users)
1. Download the latest [psps-master.zip](https://gitlab.com/fpfs/psps/-/archive/master/psps-master.zip)
2. Extract `psps-master.zip` it into a new folder of choice, such as in `Downloads/psps/`
    - Don't extract the contents directly into your Downloads folder, it'll cause a mess!
    - Note that you will need to [Reinstall](#2-Updating-psps.py) in order to get updates!

**With Git:**
1. Change Directory to where you would like the `psps` folder installed
2. Run: `git clone https://gitlab.com/fpfs/psps.git`
    - This will install an exact copy of psps.py in subdirectory `psps`

## Updating psps.py
**If you have made any changes to the `config.py` file, it will be overwritten.**

**Without Git:**
1. Backup (move or copy) any `.txt` files you have created in this folder if you want to keep them.
2. Delete the `psps` folder and its contents.
3. Install as per the [Install psps.py](#2-Install-psps.py) section

**With Git:**
1. Change Directory to where `psps.py` is installed.
2. Run: `git pull`
    - This will pull the latest changes from this repository

---
# Usage
psps.py is a command line application, you will need to use your Command Line Interface (**CLI**) to utilize it. It will not work properly if run by clicking via your Graphical User Interface. Please ensure you have [Installed Python 3](#2-Install-Python-3) before continuing.

## Running psps.py
1. Open your [Command Line Interface](#2-Command-Line-Interface-Applications-by-OS) (Hereby referred as **CLI**).\
&nbsp;**Linux**:\
&nbsp;&nbsp;Refer to your distribution's documentation\
&nbsp;**Mac**:\
&nbsp;&nbsp;1. Open Spotlight by clicking the magnifying glass in the upper right hand corner of the screen.\
&nbsp;&nbsp;2. Type in "Terminal" to search for the Terminal application.\
&nbsp;&nbsp;3. Click "Terminal", to open the Terminal application.\
&nbsp;**Windows**:\
&nbsp;&nbsp;1. Open your File Explorer (Looks like a Yellow Folder, lets' you view folders and files).\
&nbsp;&nbsp;2. Navigate to the folder you have installed psps.py into.\
&nbsp;&nbsp;3. Hold Shift, then right click on an empty spot in the installation folder.\
&nbsp;&nbsp;4. Select "Open PowerShell window here". This method conveniently allows you to skip step 2.

2. Navigate to the psps.py installation directory (folder) in your CLI.\
The below commands assuming psps.py is installed in the **Downloads** folder, in a folder named **psps**:\
&nbsp;**Linux, Mac**: `cd ~/Downloads/psps`\
&nbsp;**Windows**: `cd $HOME\Downloads\psps`

3. Test psps.py with Python3, to print the usage instructions. Run the following command in your CLI.\
&nbsp;**Linux, Mac**: `python ./psps.py`\
&nbsp;**Windows** `python .\psps.py`

4. If you are unfamiliar with using a CLI, run the wizard.\
&nbsp;**Linux, Mac**: `python ./psps.py -w`\
&nbsp;**Windows** `python .\psps.py -w`

5. Enjoy!

**Note:** If you have both Python2 and Python3 installed, you may need to call psps.py with `python3` instead of `python`.

## Technical Usage
```
usage: psps.py [-h] [-w] [-e] [-f FILE] [-n MODULENAME] [-c PLAYERCOUNT] [-p PRIORITYVALUE] [-s SNUBWEIGHT] [-d DICESIDES] [playerName ...]

A overcomplicated script for the simple task of player selection for a small pathfinder society branch.

positional arguments:
  playerName            list of player names, optionally paired with their priority and snub count
                        if priority or more values are attached to an entry, it must be put in quotes
                        order of grouping is always Name, Priority, Snub
                        
                        examples:
                        psps.py player1 player2 player3
                        psps.py 'myplayer,50,0'
                        psps.py 'yip,70,0' 'yap,50,0' 'yop,25,1'
                        psps.py 'egg,80' kobold 'simon,10,5' 

operations:
  -h, --help            show this help message and exit
  -w, --wizard          invoke the wizard to build and run a playerlist after asking you some questions
  -e, --examplefile     create a example file to be read as a player list as example_list.txt
                        this will overwrite any exsisting example_list.txt file!
  -f FILE, --file FILE  read playerlist from provided filename

optional arguments:
  -n MODULENAME, --modulename MODULENAME
                        change the name of the module being run default is Unnamed Adventure
  -c PLAYERCOUNT, --playercount PLAYERCOUNT
                        change the amount of available player slots for the module default is 4
  -p PRIORITYVALUE, --priorityvalue PRIORITYVALUE
                        change the priority value to roll under, default is 50
  -s SNUBWEIGHT, --snubweight SNUBWEIGHT
                        change the snub weight, which is priority value to add per snub, default is 10
  -d DICESIDES, --dicesides DICESIDES
                        change how many sides the die rolled has
                        default is 100, which makes 1d100
                        if a priority value is higher than the dice rolled the player will be automatically selected
```
---
# Configuration
All user variables are changable via optional command line arguments shown above in the [Technical Usage](#1-Technical-Usage) Section.

**This should be avoided as you can lose your changes**, but if you want to change the defaults permanantly, you may modify the `config.py` file.

---
# Contributions
As this is a learning project, help is welcomed, but should be approached in a way to help this project's developers to grow in skill.
If you find anything wrong, or see anything that can be improved, please report it! Thanks!

## Sniffer
There is a (crude) scent.py file which can be used with `sniffer`, you can install it with pip.
- Currently catches bad syntax
- Also prints any found # TODO lines

You can run it simply by running `sniffer` in the project directory after installation of sniffer.
